import React, { Component, } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
} from 'react-native';

class Countdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secondsRemaining: 0,
    }
    
    this.tick = this.tick.bind(this);
  }
  
  tick(){
    console.log(this.state.secondsRemaining);
    this.setState({secondsRemaining: this.state.secondsRemaining - 1});
    if (this.state.secondsRemaining <= 0) {
      clearInterval(this.interval);
    }
  }

  componentDidMount() {
    console.log(this.props.secondsRemaining );
    this.setState({ secondsRemaining: this.props.time });
    this.interval = setInterval(this.tick, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  render() {
    return (
      <Text style={{fontFamily: 'mono',fontSize: 24,}}>Time: {this.state.secondsRemaining}</Text>
    );
  }
}


export default Countdown;


'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import update from 'react-addons-update';

import { openDrawer } from '../../actions/drawer';
import { popRoute } from '../../actions/route';

import { Container, Header, Title, Content, Text, Button, Icon } from 'native-base';
import { Animated, Easing, StyleSheet, View, Image, Modal, StatusBar } from 'react-native';
import Sound from 'react-native-sound';

import myTheme from '../../themes/base-theme';

import database from '../../../assets/pokemon.json';

import Level from '../level';
import Countdown from '../countdown';


let images  = require('../../shared/image');

var {width, height} = require('Dimensions').get('window');
const COL_SIZE = 5; // 8-by-four grid
const ROW_SIZE = 5; // 8-by-four grid
var CELL_SIZE = Math.floor(width * .18); // 20% of the screen width
var CELL_PADDING = Math.floor(CELL_SIZE * .02); // 5% of the cell size
var BORDER_RADIUS = CELL_PADDING * 2;
var TILE_SIZE = CELL_SIZE;
var LETTER_SIZE = Math.floor(TILE_SIZE * .75);
var CHOICES_SIZE = 7;
const ROCKET_CHOICE = -1;
var CELL_BORDER = 0.5;

const styles = StyleSheet.create({
  container: {
    width: CELL_SIZE * COL_SIZE,
    height: CELL_SIZE * COL_SIZE,
    backgroundColor: 'transparent',
  },
  tile: {
    position: 'absolute',
    width: TILE_SIZE,
    height: TILE_SIZE,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderRightWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: '#215fb5',
  },
  given: {
    position: 'absolute',
    width: TILE_SIZE,
    height: TILE_SIZE,
    borderRadius: BORDER_RADIUS,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4d9c2d',
  },
  letter: {
    color: '#333',
    fontSize: LETTER_SIZE,
    fontFamily: 'NukamisoLite',
    backgroundColor: 'transparent',
  },
  stretch: {
    width: 40,
    height: 40
  },
  lastColumn: {
    borderWidth: 0,
  },
  background: {
    flex: 1,
    width: undefined,
    height: undefined,
    backgroundColor: '#90f9f4',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  footer: {
      width: 438,
      height: 43
  }
});

class GamePage extends Component {
    constructor(props) {
        super(props);
        var tilts = new Array(COL_SIZE * ROW_SIZE);
        for (var i = 0; i < tilts.length; i++) {
            tilts[i] = new Animated.Value(0);
        }
        // Set up initial state
        var dist = this.initGameDistribution();
        var remainPkms = JSON.parse(JSON.stringify(database));
        remainPkms.splice(dist.given.id - 1, 1);
        this.state = {
            tilt: tilts,
            distribution: dist,
            stage: 0,
            level : 0,
            pickedPokemons : [dist.given],
            remainPokemons: remainPkms,
            time: 10,
            secondsRemaining: 10,
        };

        this.tick = this.tick.bind(this);
    }

    initGameDistribution() {
        // randomize the pokemon from the list
        var remainPokemons = JSON.parse(JSON.stringify(database));
        var pIndex = Math.floor(Math.random() * remainPokemons.length);
        var pickedPokemon = remainPokemons[pIndex];
        remainPokemons.splice(pIndex, 1);
               
        var showPosition = Math.floor(Math.random() * ROW_SIZE * COL_SIZE);

        var choices = this.generateChoices(pickedPokemon);

        // Generate randomly array of 5 other pokemon and place in 
        var cPositions = this.generateChoicesTiles(showPosition).sort(function(a,b) { return parseFloat(a) - parseFloat(b)});

        var distribution = {
            given: pickedPokemon,
            givenPosition: showPosition,
            choices: choices,
            choicesPosition: cPositions
        }
        return distribution;
    }

    componentDidMount() {
        // this.setState({ secondsRemaining: this.props.time });
        this.interval = setInterval(this.tick, 1000);
    }
    
    
    componentWillMount() {
        clearInterval(this.interval);
    }
    

    popRoute() {
        this.props.popRoute();
    }

    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps should update  state kkk  '+ JSON.stringify(this.state));
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    playSuccessfulSound() {
        //var s = new Sound('../../../assets/sound/successful.mp3', Sound.MAIN_BUNDLE, (e) => {
        var s = new Sound('successful.mp3', Sound.MAIN_BUNDLE, (e) => {
        if (e) {
            console.log('error', e);
        } else {
            console.log('duration', s.getDuration());
            s.play();
        }
        });
    }

    playFailedSound() {
        //var s = new Sound('../../../assets/sound/failed.wav', Sound.MAIN_BUNDLE, (e) => {
        var s = new Sound('failed.wav', Sound.MAIN_BUNDLE, (e) => {
        if (e) {
            console.log('error', e);
        } else {
            console.log('duration', s.getDuration());
            s.play();
        }
        });
    }

    pickNewQuestion(){
        var pIndex = Math.floor(Math.random() * (this.state.remainPokemons.length));
        var pickedPokemon = this.state.remainPokemons[pIndex];
        this.state.remainPokemons.splice(pIndex,1);
        return pickedPokemon;
    }

    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex ;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    generateChoices(pokemon) {
        var choices = [];

        var choiceSize = CHOICES_SIZE;
        if(this.state != undefined ) {
             choiceSize += this.state.level;
        }
        while(choices.length < (choiceSize - 1)) {
            // randomize the pokemon from the list
            var randomPokemon = Math.floor(Math.random() * database.length);
            if((database[randomPokemon].id != pokemon.id) && !choices.includes(database[randomPokemon])) {
                choices.push(database[randomPokemon]);
                if(database[randomPokemon] == undefined) {
                    console.log('undefined x ' + randomPokemon);
                }   
                
            }
        }
        // Add the pokemon right after the pIndex;
        if((pokemon.evolTo != 0) && !(pokemon.evolTo instanceof Array)){
            choices.push(database[pokemon.evolTo - 1]);
            if(database[pokemon.evolTo - 1] == undefined) {
                console.log('undefined a ' + pokemon.evolTo);
            }
        } else if(pokemon.id == database.length){
            choices.push(-1);
        } else if(pokemon.evolTo instanceof Array) {
            // !!!!!!!!!!!!!!Evee!!!!!!!!!!!!!!
            var randomEvolEvee = Math.floor(Math.random() * 3) + pokemon.id;
            choices.push(database[randomEvolEvee]);
            if(database[randomEvolEvee] == undefined) {
                console.log('undefined b ' + pokemon.evolTo);
            }
        } else  {
            choices.push(-1);
        }
        var shuffleResult = this.shuffle(choices);
        return choices;
    }

    generateChoicesTiles(showPosition){
        var tiles = [];
        
        var choiceSize = CHOICES_SIZE;
        if(this.state != undefined) {
             choiceSize += this.state.level;
        }
        while(tiles.length < choiceSize) {
            // randomize the pokemon from the list
            var randomPosition = Math.floor(Math.random() * COL_SIZE * COL_SIZE);
            if((randomPosition != showPosition) && (!tiles.includes(randomPosition))) {
                tiles.push(randomPosition);
            }
        }
        return tiles;
    }

    distribute() {
        // randomize the pokemon from the list
        var pickedPokemon = this.pickNewQuestion();
        var showPosition = Math.floor(Math.random() * ROW_SIZE * COL_SIZE);
        var choices = this.generateChoices(pickedPokemon);

        // Generate randomly array of 5 other pokemon and place in 
        var cPositions = this.generateChoicesTiles(showPosition).sort(function(a,b) { return parseFloat(a) - parseFloat(b)});

        var distribution = {
            given: pickedPokemon,
            givenPosition: showPosition,
            choices: choices,
            choicesPosition: cPositions
        }
        return distribution;
    }

    renderEmptyTile(id, style) {
        return <Animated.View key={id} style={[styles.tile, style]}
                    onStartShouldSetResponder={() => this.clickTile(id)}>
                </Animated.View>;
    }

    renderQuestionTile(id, style, pkm) {
        return <Animated.View key={id} style={[styles.given, style]}
                    onStartShouldSetResponder={() => this.clickTile(id)}>
                    <Image
                        style={styles.stretch}
                        source={images[pkm.id]}  
                    />
            </Animated.View>;
    }

    renderBlackTile(id, style, pkm) {
        if(pkm != -1) {
            return <Animated.View key={id} style={[styles.tile, style]}
                    onStartShouldSetResponder={() => this.clickTile(id)}>
                    <Image
                            style={styles.stretch}
                            source={images[pkm.id]}  
                        />
                </Animated.View>;
        } else {
            return <Animated.View key={id} style={[styles.tile, style]}
                    onStartShouldSetResponder={() => this.clickTile(id)}>
                    <Image
                            style={styles.stretch}
                            source={require('../../../assets/pokemon/rocket.png')}  
                        />
                </Animated.View>;
        }
    }

    renderGameTiles() {
        var result = [];
        var qIndex = 0;;
        for (var row = 0; row < ROW_SIZE; row++) {
            for (var col = 0; col < COL_SIZE; col++) {
                var id = row * COL_SIZE + col;
                var tilt = this.state.tilt[id].interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '-30deg']
                });
                var style = {
                left: col * CELL_SIZE + CELL_BORDER * 2,
                top: row * CELL_SIZE + CELL_BORDER * 2,
                transform: [{perspective: CELL_SIZE * 8},
                            {rotateX: tilt}]
                };               
                
                if(id == this.state.distribution.givenPosition) {
                    result.push(this.renderQuestionTile(id, style, this.state.distribution.given));
                } else if(this.state.distribution.choicesPosition.includes(id)) {
                    result.push(this.renderBlackTile(id,style, this.state.distribution.choices[qIndex]));
                    qIndex++;
                } else {
                    result.push(this.renderEmptyTile(id, style));
                }
                
            }
        }
        return result;
    }

    next() {
        this.playSuccessfulSound();
        // Passed 20 stages, go next level
        if(23 == (CHOICES_SIZE + this.state.level)){
            alert('CHAMPION!!!!!!!!!!!!!!');
            return;
        }
        console.log('pickedPokemons ' + this.state.pickedPokemons.length);
        console.log('level ' + this.state.level);
        if(this.state.pickedPokemons.length == 20) {
            var dist = this.initGameDistribution();
            var remainPkms = JSON.parse(JSON.stringify(database));
            remainPkms.splice(dist.given.id - 1, 1);
            console.log('stage ' + this.state.stage);
            var newState = update(this.state, {tilt: {$set: this.state.tilt}, 
                        distribution: {$set:dist}, 
                        level: {$set: this.state.level + 1},
                        stage: {$set: this.state.stage + 1}, 
                        pickedPokemons:{$set: [dist.given]} ,
                        remainPokemons:{$set: remainPkms}});
            this.setState(newState);
            return;

        }
        if(this.state.remainPokemons.length == 0) {
            alert('YOU HAVE JUST DESTROYED THE GAME');
            return;
        }
        var dist = this.distribute();
        var newState = update(this.state, {tilt: {$set: this.state.tilt}, 
                        distribution: {$set:dist}, 
                        level: {$set: this.state.level},
                        stage: {$set: this.state.stage + 1}, 
                        pickedPokemons:{$push: [dist.given]} ,
                        remainPokemons:{$set: this.state.remainPokemons},
                        secondsRemaining: {$set: 10},
                    });
                        

        this.setState(newState);
    }

    fail() {
        this.playFailedSound();
        // stop here you gays
    }
    clickTile(id) {
        var tilt = this.state.tilt[id];
            tilt.setValue(1); // mapped to -30 degrees
            Animated.timing(tilt, {
            toValue: 0, // mapped to 0 degrees (no tilt)
            duration: 250, // milliseconds
            easing: Easing.quad // quadratic easing function: (t) => t * t
        }).start();

        // Check whether tap the correct cell or not
        var cIndex = this.state.distribution.choicesPosition.indexOf(id);
        if(this.state.distribution.choicesPosition.includes(id)) {
            if(this.state.distribution.choices[cIndex] == ROCKET_CHOICE){
                if(this.state.distribution.given.evolTo == 0) {
                    this.next();
                } else {
                    this.fail();
                }
                return;
            }
            if(this.state.distribution.given.evolTo == this.state.distribution.choices[cIndex].id) {
                this.next();
                
            } else if ((this.state.distribution.given.evolTo instanceof Array)
                        && this.state.distribution.given.evolTo.includes(this.state.distribution.choices[cIndex].id)) {
                this.next();
            }
            else {
                this.fail();
            }
        }
        
    }

    tick(){
        console.log(this.state.secondsRemaining);
        // this.setState({secondsRemaining: this.state.secondsRemaining - 1});
        var newState = update(this.state, {
                        secondsRemaining: {$set: this.state.secondsRemaining - 1},
                    });
                        

        this.setState(newState);

        if (this.state.secondsRemaining <= 0) {
        clearInterval(this.interval);
        }
    }

    render() {
        return (
            <Container theme={myTheme} style={{backgroundColor: '#7ccdca'}}>
                <View style={styles.background} >
                    <StatusBar hidden={true} />
                    <View style={{flex: 1,flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#85f2f3',}}>
                        <View style={{flex: 1,}}>
                            <Image source={require('../../../images/bg_level.png')} style={{width: 128, height: 63,}}>
                                <Level stage={this.state.stage} level={this.state.level} />
                            </Image>
                        </View>
                        <View style={{flex: 2, alignItems: 'center', justifyContent: 'center',}}>
                            <Image source={require('../../../images/logo_small.png')} style={{width: 246, height: 40}} />
                        </View>
                    </View>
                    <View style={{flex: 5, justifyContent: 'center',}}>
                        <View style={{position: 'relative', top: 5, flex: 1, flexDirection: 'row'}}>
                            <Text style={{flex: 1,fontFamily: 'mono',fontSize: 24,color: '#000000'}}>Time: {this.state.secondsRemaining}</Text>
                            <View style={{flex: 2, flexDirection: 'row', position: 'absolute', right: 0}}>
                                <Image source={require('../../../images/pause.png')} style={{width: 35, height: 35}} />
                                <Image source={require('../../../images/home.png')} style={{width: 35, height: 35, marginLeft: 10,}} />
                            </View>
                        </View>
                        <View style={styles.container}>
                            {this.renderGameTiles()}
                        </View>
                    </View>
                    <View style={{flex: 1,justifyContent: 'flex-end'}}>
                        <Image source={require('../../../images/bg_grass.png')} style={styles.footer} />
                    </View>
                </View>
            </Container>
        )
    }
}

function bindAction(dispatch) {
    return {
        openDrawer: ()=>dispatch(openDrawer()),
        popRoute: () => dispatch(popRoute())
    }
}

export default connect(null, bindAction)(GamePage);

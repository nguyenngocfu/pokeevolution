
'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Container, Header, Title, Content, Text, Button, Icon } from 'native-base';
import { Animated, Easing, StyleSheet, View, Image} from 'react-native';

class Level extends Component {
  constructor(props) {
    super(props);
    this.state = {
      level: 0,
      styleBar: {
        backgroundColor: '#c7e500',
        borderRadius: 2,
        width: 0,
        height: 3,
      }
    }
  }
  componentWillReceiveProps(nextProps) {
    this.state.styleBar.width += 5;
    
    this.state.level = nextProps.level;

    if (nextProps.level > this.props.level) {
      this.state.styleBar.width = 5;
    }
  
  }  
  
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'flex-end', marginLeft: 10, marginBottom: 10,}}>
        <Text style={styles.text}>Level {this.state.level}</Text>
        <View>
          <View style={styles.wrapLevel}>
            <Text style={this.state.styleBar}></Text>
          </View>
        </View>
      </View>
    );
  }

}

var styles = StyleSheet.create({
  wrapLevel: {
    backgroundColor: '#098500',
    borderRadius: 2,
    borderWidth: 2,
    borderColor: '#000000',
    width: 100,
    height: 7,
    marginTop: 5,
  },
  text: {
    color: '#000000',
    fontFamily: 'mono',
    fontSize: 18,
    marginLeft: 10,
  }
});

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    popRoute: () => dispatch(popRoute())
  }
}

export default connect(null, bindAction)(Level);

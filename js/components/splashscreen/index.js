'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'native-base';
import { StyleSheet, Text, Image, TouchableHighlight, StatusBar } from 'react-native';
import { replaceRoute } from '../../actions/route';

class SplashPage extends Component {
  constructor(props) {
    super(props);
  }

  replaceRoute(route) {
    this.props.replaceRoute(route);
  }

  render() {
    return (
      <Image
        source={require('../../../images/bg.jpg') }
        style={styles.container}
      >
        <View style={{flex: 3, justifyContent: 'center',}}>
          <StatusBar hidden={true} />
          <Image 
            source={require('../../../images/logo.png') }
            style={styles.logo}
          />
        </View>
        <View style={{flex: 1}}>
          <Image 
            source={require('../../../images/logo_text.png') }
            style={styles.text}
          />
        </View>
        <View style={{flex: 1}}>
          <TouchableHighlight  onPress={() => this.replaceRoute('gamePage')}>
            <Image
              style={styles.button}
              source={require('../../../images/play.png')}
            />
          </TouchableHighlight>
        </View>
      </Image>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: undefined,
    height: undefined,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  logo: {
    width: 285,
    height: 287,
  },
  text: {
    width: 247,
    height: 94,
  },
  button: {
    width: 152,
    height: 58,
  }, 
  
});

function bindActions(dispatch){
    return {
        replaceRoute:(route)=>dispatch(replaceRoute(route))
    }
}

export default connect(null, bindActions)(SplashPage);
